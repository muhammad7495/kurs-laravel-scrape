<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bukuku Scrapper</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <script src="https://cdn.tailwindcss.com"></script>
    </head>
    <body class="antialiased">
        <div class="container mx-auto">
            <div class="grid grid-cols-1">
                <div class="flex justify-center my-8">
                    <form id="clearCurrency" method="post" action="{{ route('clear-currency') }}">
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" type="submit">
                            Clear Currency Data
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.7.1.min.js" type="javascript"></script>
        <script>
            $('#clearCurrency').on('submit', (e) => {
                alert('Proses berhasil ditambahkan pada Job');
                e.preventDefault();
            })
        </script>
    </body>
</html>
