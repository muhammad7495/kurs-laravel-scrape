<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Goutte\Client;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $client = new Client();
            $website = $client->request('GET', 'https://kursdollar.org');
            $indonesia = $website->filter('body > div.container > main > div.row')->slice(1)->filter('div > table > tr')->slice(1)->filter('td')->slice(1)->text();
            $word = $website->filter('body > div.container > main > div.row')->slice(1)->filter('div > table > tr')->slice(1)->filter('td')->slice(2)->text();
            $rates = $website->filter('body > div.container > main > div.row')->slice(1)->filter('div > table > tr')->slice(3, 20)
                ->each(function ($nodes) {
                    $result = (array) $nodes->filter('td')->each(function ($node, $i) {
                        if ($i > 0) {
                            if ($node->text()) {
                                return floatval($node->text());
                            }
                        } else {
                            return substr($node->text(), 0, 3);
                        }
                    });
                    return (object) [
                        'currency' => $result[0],
                        'buy' => $result[1],
                        'sell' => $result[2],
                        'average' => $result[3],
                        'world_rate' => $result[4]
                    ];
                });
            $scrapedJson = array(
                "meta" => array(
                    "date" => date('d-m-Y'),
                    "day" => date('l'),
                    "indonesia" => $indonesia,
                    "word" => $word,
                ),
                "rates" => $rates
            );
            Storage::disk('local')->put('scrape/' . date('d-m-Y--H:i:s') . '.json', json_encode($scrapedJson));
            // })->cron('*/7 * * * *');
        })->everyFiveSeconds();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
